# Tatum Infrastructure

Design for highly available Infrastructure

We have the Docker container which would run in EC2 instances for that, and this task is executed in a private subnet, talking to the outside world via the NAT gateway in the public subnet. The ALB in the public subnet funnels HTTP requests to the task, and the response is coming via the NAT gateway. Pretty simple.

Now clicking this setup together in the AWS console is not so difficult, but with Terraform, this was the chosen infrastructure to turn into code - or to be more specific: HCL and it is faster. 

### Overall Architecture of the Infrastructure

![Screenshot of store Architecture GitHub to Server](./Diagram/architecture.png)
Source: https://aws.amazon.com/blogs/aws/new-application-load-balancer-simplifies-deployment-with-weighted-target-groups

## VPC

- The VPC with attached internet gateway is configured with just a few lines of code,
- I added public and private subnets (per availability zone) for high availability
- Then, I configured the routing table for the public subnet, going through the internet gateway 
- For the private subnets, we need to attach NAT gateways for communication with the outside world, which also need an ElasticIP associated
- Now we can create the route table for the private subnet, where traffic is routed through the NAT gateway
- And with that, we have setup a new VPC with both, public and private subnets, which we will use to setup the cluster: [VPC Setup and configuration](https://gitlab.com/t6238/application/-/blob/main/Infrastructure/main.tf) 

## Security Group

- I created one security group for the ALB that allows only access via TCP ports 80 (HTTP) and port 22 (SSH), which looks like [this](https://gitlab.com/t6238/application/-/blob/main/Infrastructure/main.tf) 

## Elastic Compute Cloud (EC2)

First,  we use terraform to create EC2 and everything the EC2 server will need like the VPC, Security Group and so on. From the terraform, direct install of docker is done and run docker image which converts the CSV from s3 bucket to html with python. [Here](https://gitlab.com/t6238/application/-/blob/main/App/start_script.sh) 

I converted the downloaded CSV file that was given which I placed in my AWS S3 to HTML with python using pandas
The converted html was copied into EC2 instance in the directory /var/www/html after I installed nginx

I used terraform for my infrastructure as code and for high availability, I used a load balancer with no single point of failure

![Screenshot of running Instance](./Diagram/Running_EC2.png)


## Application Load Balancer
The configuration we have, it includes a reference to an ALB that will manage the distribution of requests to all the running tasks.

The definition of the ALB is pretty straightforward, it consists of two listeners, one for HTTP,  which funnels traffic to the target group. This target group is later used by the EC2 service to propagate the available tasks to.

One thing for the ALB that I did not create with Terraform is the TSL certificate for HTTPS, the ARN for this I set as variable and passed it to the resource.

![Screenshot of running Application Load Balancer](./Diagram/Load_Balancer.png)

Here is my URL: http://tatum-lb-1096959918.us-east-1.elb.amazonaws.com/


# Running Terraform 

# Terraform - AWS Terraform to spinup EC2 and run script inside the instance

- The terraform script helps to create and configure the VPC, Security Group, ECR, ECS and Application Load Balancer  and immefiately after creation, a script will run inside the instance
- The script has a remote state in AWS S3 which it stores it states in it
- The repo contains code for GitLab CI as the CI/CD tool where it help to automate the work 

![Screenshot of terraform state](./Diagram/Terraform_State.png)
<Here is a diagram showing that the terraform state was stored in s3>


## How to use
This is done with [Gitlab CI](https://gitlab.com/t6238/application/-/blob/main/.gitlab-ci.yml)

Pipeline can be seen [here](https://gitlab.com/t6238/application/-/pipelines)

```
$terraform init -reconfigure
$terraform plan 
$terraform apply 
$terraform destroy #To destroy the terraform infrastructure, which is option, it can only be manually done
```
