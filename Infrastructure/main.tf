provider "aws" {
  region = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key 
}


terraform {
  required_version = ">= 1.0.7"
  backend "s3" {
    bucket  = "tatum-terraform-backend-store"
    encrypt = true
    key     = "terraform.tfstate"
    region  = "us-east-1"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name           = "terraform-state-lock-dynamo"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "DynamoDB Terraform State Lock Table"
  }
}


resource "aws_instance" "instance" {
  ami                         = "ami-0e472ba40eb589f49"
  instance_type               = "t2.micro"
  key_name                    = "tatumkey"
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = "subnet-5a32d46b"
  associate_public_ip_address = true

  root_block_device {
    volume_size           = 50
    delete_on_termination = true
  }

  tags = {
    Name = "Tatum-task"
  }

  provisioner "remote-exec" {
    inline = [
      "cd /home/ubuntu/",
      "git clone https://gitlab.com/t6238/application.git",
      "cd application",
      "chmod +x app/start_script.sh",
      "./start.sh",
      "sleep 6000",
    ]

    connection {
      host        = self.public_ip
      type        = "ssh"
      private_key = "${tls_private_key.tatum.private_key_pem}"
      user        = "ubuntu"
      timeout     = "1m"
    }

  } 
}
  

  resource "tls_private_key" "tatum" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_security_group" "sg" {
  name        = "Tatum-sg"
  description = "Restrictions for Tatum Server"

  ingress {
    from_port   = 22
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "vpc-79698604"

  tags = {
    Name = "Tatum-Server-SG"
  }
}

output "instance-private-ip" {
  value = aws_instance.instance.private_ip
}

output "instance-public-ip" {
  value = aws_instance.instance.public_ip
}

#Load-balancer
resource "aws_lb" "tatum_lb" {
  name               = "tatum-lb"
  internal           = false
  load_balancer_type = "application"
  ip_address_type    = "ipv4"
  security_groups    = ["${aws_security_group.sg.id}"]
  subnets            = ["subnet-5a32d46b", "subnet-7d51821b", "subnet-b2fd69bc"]
  depends_on         = [aws_security_group.sg]
  tags = {
    Name = "tatum-lb"
  }
}

resource "aws_lb_target_group" "tatum_target_grp" {
  name        = "${aws_lb.tatum_lb.name}-tatum-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "vpc-79698604"
  target_type = "instance"
  depends_on  = [aws_lb.tatum_lb]

  health_check {
      enabled             = "true"
      interval            = 10
      path                = "/"
      port                = 80
      protocol            = "HTTP"
      healthy_threshold   = 5
      unhealthy_threshold = 3
      matcher             = 200
  }

    stickiness {
      type                = "lb_cookie"
      cookie_duration     = 1
      enabled             = true
  }


  tags = {
      Name = "${aws_lb.tatum_lb.name}-trg-grp"
  }
}

resource "aws_lb_listener" "app_http_listener" {
  load_balancer_arn = "${aws_lb.tatum_lb.arn}"
  port              = 80
  protocol          = "HTTP"
  depends_on        = [aws_lb.tatum_lb]
  default_action {
    target_group_arn = "${aws_lb_target_group.tatum_target_grp.arn}"
    type             = "forward"
  }
}

