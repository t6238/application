#!/bin/bash

#Installing Docker
apt-get update -y 
apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
systemctl start docker

# Running Docker 
docker build -t $ECR_REGISTRY/$ECR_REPOSITORY:$IMAGE_TAG . #build image according to the commit
docker push $ECR_REGISTRY/$ECR_REPOSITORY:$IMAGE_TAG #push image to ECR
echo "::set-output name=image::$ECR_REGISTRY/$ECR_REPOSITORY:$IMAGE_TAG"
docker run -d --name tatum -p 80:80 $ECR_REGISTRY/$ECR_REPOSITORY:$IMAGE_TAG 
docker system prune -f # remove dangling images
