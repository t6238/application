import pandas as pd
 
# to read csv file n
a = pd.read_csv("s3://covid-19-master/csse_covid_19_data/csse_covid_19_daily_reports_us/01-01-2021.csv")
 
# to save as html file
# named as "Table"
a.to_html("Tatumtask.html")
 
# assign it to a
# variable (string)
html_file = a.to_html()
